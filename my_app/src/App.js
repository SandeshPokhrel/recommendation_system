import React from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import './App.css';
import {Login} from "./components/Login";
import Profile from "./components/Profile";
import {Register} from "./components/Register";
// import PrivRoutes from "./utils/privRoute";


function App() {
  
  return (
   <div className = "app">
    <Router>
        <Routes>
          {/* <Route element = {<PrivRoutes/>}> */}
          <Route element = {<Login/>} path = "/" exact/>
         <Route element = {<Profile/>} path = "/Profile" />
          {/* </Route> */}
          <Route element = {<Register/>} path = "/Register" />
        </Routes>
    </Router>

    
   </div>
  );
}

export default App;
