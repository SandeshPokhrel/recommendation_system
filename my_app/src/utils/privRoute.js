import {Outlet, Navigate} from 'react-router-dom';

const privRoutes = () => {
    let auth={'token':false};
  return (
    auth.token ? <Outlet/> : <Navigate to = "/Login"/>
  )
}
export default privRoutes;