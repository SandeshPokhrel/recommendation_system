import React from 'react'
import { useState, useEffect } from 'react'
import {useNavigate} from 'react-router-dom'
const Profile = () => {
  const [movies, setMovies] = useState([]);// all the movies as obj: id, title, year, cast etc
  //const [movie,setMovieID] = useState('0');
  const [ratings, setRating] = useState({});
  const navigate = useNavigate();
 
  // const handleMovieChange = (e)=>{
  //   setMovieID(e.target.value);
  // }

  //useEffect to render all the updated content in display each time refreshed
  useEffect(()=>{
    fetch('http://localhost:8000/api/movietorecommend/' , {  
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + localStorage.getItem('access'),
      'Content-Type': 'application/json',
    }
  })
    .then((response)=>{
      return response.json();
    })
    .then((data)=>{
      setMovies(data);
      //for iteration over all the movie object and set rating to 0 for each movie with special id//
      setRating(data.reduce((accumulator_obj, movie)=>{
        accumulator_obj[movie.id] = '0';
        return accumulator_obj;
      },{}));
      console.log(data);
    });
  
  }, []);

  // to handle the input field in the movie card "for each" !!!
  const handleRatingChange = (e, id)=>{
    setRating(prevRatings =>({
      ...prevRatings,
      [id]: e.target.value
    }));
}
  const submitRating = (id)=>{
    const rating = ratings[id];
    fetch('http://localhost:8000/api/ratemovie/', {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('access'),
        'Content-Type': 'application/json'
      },
      body:JSON.stringify({movie:id, rating:rating}, )}
      
  )
  
  .then((response)=>{
      return response.json();
  }).then((data)=>{
    console.log(data);
  }) 
  }
  return(
    <div>
      {movies.map(movie=>(
        <div className="card" key = {movie.id}>
        <div className="card-body">
        <h5 className="card-title">{movie.Title}</h5>
        <h6 className="card-year" >{movie.Year} | Genre: {movie.Genre1}, {movie.Genre2}</h6>
        <h6 className="card-actor" >{movie.Cast1}</h6>
        <form onSubmit ={()=>submitRating(movie.id)}>
            <input type = "text" id = {movie.id} value = {ratings[movie.id]} onChange={(e)=>handleRatingChange(e,movie.id)}></input>
            <input type = "submit" value = "submit"/>
        </form>
        </div>
        </div>
      ))
      }
    </div>
    
      
  );
  }
  export default React.memo(Profile);

